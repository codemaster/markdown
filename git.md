## 1.名词解释
- GIT:分布式版本控制系统
- 版本库：又叫仓库(Repository),简单理解为一个目录，目录里的所有文件都被git管理着，每个文件的修改，都被git跟踪，以便在任意时刻查看、恢复历史记录




## 2.命令
- `git init`:将当前目录初始化为git可以管理的仓库,此时会在当前目录创建一个`.git`的目录，用于追踪管理版本库
- `git add <文件名>`：将文件添加到暂存区
- `git commit -m "message"`：将暂存区文件添加到仓库中
- `git config`：查看当前git配置信息
	- `git config --global user.name "name"`、`git config --global user.email "email"`:设置提交代码的用户信息，不加`global`只对当前仓库生效
- `git status`：查看仓库当前状态
- `git clone`：拷贝远程仓库至本地
- `git diff <文件名>`：比较文件的不同，为暂存区和工作区的不同;`git diff --cached [文件名]`：比较暂存区和上一次提交的不同
- `git log [--pretty=oneline]`：查看历史提交记录
- `git reflog`：查看所有操作历史
- `git blame <文件名>`：查看指定文件的历史修改记录
- `git reset HEAD~<数字>`:版本回退，数字表示回退几个版本，比如数字1表示回退一个版本，
	- `git reset <版本号>`:指定版本号回退
- `git restore <文件名>`：撤销工作区的修改
	- `git restore --staged <文件名>`：撤销暂存区的修改
- `git rm <文件名>`：将文件从工作区和暂存区删除，删除后需要commit
	- `git rm --cached <文件名>`：仅删除暂存区的文件，或者说从跟踪清单中删除
### 远程仓库
- `ssh-keygen -t rsa -C "email"`：创建SSH Key
- `git remote add origin <远程仓库地址>`:将本地仓库链接到远程仓库，关联一个远程库时必须给远程库指定一个名字，origin是默认习惯命名；
- `git remote rm <远程仓库名字>`：解除本地仓库与远程仓库的关联
- `git push [-u] origin master `：将本地仓库的master分支推送到远程仓库origin上。第一次推送需要加上参数-u，会将本地仓库和远程仓库关联起来。
- `git clone <远程仓库地址>`：克隆远程仓库至本地；
- `git pull <远程主机名> <远程分支名>:<本地分支名>`:拉取远程仓库，

### 分支管理
暂时不需要

## 3.注意
- 所有的版本控制系统只能追踪文本文件的改动，不能追踪二进制文件，包括word文件。
- git工作区、暂存区、版本库的区别
	- **工作区**：就是电脑中能看到的目录
	- **版本库**：工作区的隐藏目录`.git`，这个不算工作区，而是git的版本库，包括三个关键位置:
		- 暂存区：包含文件目录树和对象索引
		- master分支：每个版本库必有一个master分支，另外有一个HEAD游标指向当前工作分支，
		- 对象库：Index,	实际位于`.git/objects`里面包含了创建的各种对象及内容
	- 当对工作区修改（删除）的文件执行`git add`命令时，暂存区的目录树被更新，同时工作区的修改内容会被写入到对象库中一个新的对象中，而该对象的ID会被记录在暂存区的文件索引中
	- 当执行提交操作`git commit`时，暂存区的目录树写到版本库（对象库）中，master 分支会做相应的更新。即 master 指向的目录树就是提交时暂存区的目录树。

![](https://gitee.com/zyanjun/pictures/raw/master/base/git_keyword.png)