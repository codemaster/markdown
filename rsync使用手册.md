### 1.基本参数

<!--其中`source`表示原目录,`destination`表示目标目录-->

1. ==-r==, 表示递归,即包含子目录.

   ```bash
   $ rsync -r source destination
   ```

   如果有多个文件或目录需要同步，可以写成下面这样。

   ```bash
   $ rsync -r source1 source2 destination
   ```

2. ==-a==, 除了可以递归同步外, 还可以同步元信息(修改时间,权限等), 由于 rsync 默认使用文件大小和修改时间决定文件是否需要更新，所以`-a`比`-r`更有用。下面的用法才是常见的写法。

   ```bash
   $ rsync -a source destination
   ```

   目标目录`destination`如果不存在，rsync 会自动创建。执行上面的命令后，源目录`source`被完整地复制到了目标目录`destination`下面，即形成了`destination/source`的目录结构。

   如果只想同步源目录`source`里面的内容到目标目录`destination`, 则需要在源目录后面加上斜杠。

   ```bash
   $ rsync -a source/ destination
   ```

3. ==-n==, 模拟命令执行的结果

4. ==-v==, 将结果输出到终端

5. ==--delete==

   默认情况下,rsync只确保将源目录中的内容复制到目标目录中,`--delete`参数则是将目标目录成为源目录的副本

   ```bash
   $ rsync -av --delete source/ destination
   ```

   

6. ==--exclude==, 排除某些文件或者目录

   ```bash
   $ rsync -av --exclude '*.txt' source/ destination
   ```

7. ==--include==, 指定必须同步的文件,与--exclude搭配使用

   ```bash
   $ rsync -av --include="*.txt" --exclude='*' source/ destination
   ```

   上面命令表示指定同步时，排除所有文件，但是会包括 TXT 文件。

### 2. 远程同步

#### 1.ssh协议

1. 将本地内容同步到远程服务器

   ```bash
   $ rsync -av source/ username@remote_host:destination
   ```

2. 将远程内容同步到本地。

   ```bash
   $ rsync -av username@remote_host:source/ destination
   ```

#### 2.rsync协议

略

### 3.其他

+ 本机使用 rsync 命令时，可以作为==cp==和==mv==命令的替代方法，将源目录同步到目标目录。

### 4. 全部参数

```
-v, --verbose          详细模式输出。
-q, --quiet            精简输出模式。
-c, --checksum         打开校验开关，强制对文件传输进行校验。
-a, --archive          归档模式，表示以递归方式传输文件，并保持所有文件属性，等于 -rlptgoD。
-r, --recursive        对子目录以递归模式处理。
-R, --relative         使用相对路径信息。
-b, --backup           创建备份，也就是对于目的已经存在有同样的文件名时，将老的文件重新命名为 ~filename。可以使用 --suffix 选项来指定不同的备份文件前缀。
--backup-dir           将备份文件（~filename）存放在在目录下。
-suffix=SUFFIX         定义备份文件前缀。
-u, --update           仅仅进行更新，也就是跳过所有已经存在于 DST，并且文件时间晚于要备份的文件。（不覆盖更新的文件。）
-l, --links            保留软链结。
-L, --copy-links       想对待常规文件一样处理软链结。
--copy-unsafe-links    仅仅拷贝指向 SRC 路径目录树以外的链结。
--safe-links           忽略指向 SRC 路径目录树以外的链结。
-H, --hard-links       保留硬链结。
-p, --perms            保持文件权限。
-o, --owner            保持文件属主信息。
-g, --group            保持文件属组信息。
-D, --devices          保持设备文件信息。
-t, --times            保持文件时间信息。
-S, --sparse           对稀疏文件进行特殊处理以节省 DST 的空间。
-n, --dry-run          显示哪些文件将被传输（新增、修改和删除）。
-W, --whole-file       拷贝文件，不进行增量检测。
-x, --one-file-system  不要跨越文件系统边界。
-B, --block-size=SIZE  检验算法使用的块尺寸，默认是 700 字节。
-e, --rsh=COMMAND      指定使用 rsh, ssh 方式进行数据同步。
--rsync-path=PATH      指定远程服务器上的 rsync 命令所在路径信息。
-C, --cvs-exclude      使用和 CVS 一样的方法自动忽略文件，用来排除那些不希望传输的文件。
--existing             仅仅更新那些已经存在于 DST 的文件，而不备份那些新创建的文件。
--delete               删除那些 DST 中 SRC 没有的文件。
--delete-excluded      同样删除接收端那些被该选项指定排除的文件。
--delete-after         传输结束以后再删除。
--ignore-errors        即使出现 IO 错误也进行删除。
--max-delete=NUM       最多删除 NUM 个文件。
--partial              保留那些因故没有完全传输的文件，以便实现断点续传。
--force                强制删除目录，即使不为空。
--numeric-ids          不将数字的用户和组 ID 匹配为用户名和组名。
--timeout=TIME         IP 超时时间，单位为秒。
-I, --ignore-times     不跳过那些有同样的时间和长度的文件。
--size-only            当决定是否要备份文件时，仅仅察看文件大小而不考虑文件时间。
--modify-window=NUM    决定文件是否时间相同时使用的时间戳窗口，默认为 0。
-T --temp-dir=DIR      在 DIR 中创建临时文件。
--compare-dest=DIR     同样比较 DIR 中的文件来决定是否需要备份。
--progress             显示传输过程。
-P                     等同于 -partial -progress。
-z, --compress         对备份的文件在传输时进行压缩处理。
--exclude=PATTERN      指定排除不需要传输的文件模式。
--include=PATTERN      指定不排除而需要传输的文件模式。
--exclude-from=FILE    排除 FILE 中指定模式的文件。
--include-from=FILE    不排除 FILE 指定模式匹配的文件。
--version              打印版本信息。
--address              绑定到特定的地址。
--config=FILE          指定其他的配置文件，不使用默认的 rsyncd.conf 文件。
--port=PORT            指定其他的 rsync 服务端口。
--blocking-io          对远程 shell 使用阻塞 IO。
--stats                给出某些文件的传输状态。
--log-format=formAT    指定日志文件格式。
--password-file=FILE   从 FILE 中得到密码。
--bwlimit=KBPS         限制 I/O 带宽，KBytes per second。
-h, --help             显示帮助信息。
```