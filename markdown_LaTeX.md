## 1、上标、下标、求和、括号

|       | markdown语法（输入在两个$$之间）                              | 显示                                                   |
| ----- | -------------------------------------------------- | ---------------------------------------------------- |
| 上标    | x^2、 x^y 、e^3                                      | $x^2、 x^y 、e^3    $                                  |
| 下标    | x_0、a_1、T_1                                        | $x_0、a_1、T_1 $                                       |
| 求和    | \sum                                               | $\sum$                                               |
| 求和上下标 | \sum_0^3 、\sum_0^{\infty} 、\sum_{-\infty}^{\infty} | $\sum_0^3 、\sum_0^{\infty} 、\sum_{-\infty}^{\infty}$ |
| 中括号   | [s(s+1)-(s-1)(s-2)]                                | $[s(s+1)-(s-1)(s-2)]$                                |
| 花括号   | \lbrace x-y \rbrace                                | $\lbrace x-y \rbrace$                                |

## 2、三角函数、指数、对数

|     | markdown语法（输入在两个$$之间） | 显示          |
| --- | --------------------- | ----------- |
| sin | \sin(x)               | $\sin(x)$   |
| cos | \cos(x)               | $\cos(x)$   |
| tan | \tan(x)               | $\tan(x)$   |
| log | \log_2 10             | $\log_2 10$ |
| ln  | \ln2                  | $\ln2$      |

## 3、运算符

|       | markdown语法（输入在两个$$之间）                 | 显示                                      |
| ----- | ------------------------------------- | --------------------------------------- |
| 乘     | \times                                | $\times$                                |
| 除     | \div                                  | $\div$                                  |
| 加减    | \pm                                   | $\pm$                                   |
| 减加    | \mp                                   | $\mp$                                   |
| 求和    | \sum                                  | $\sum$                                  |
| 求积    | \prod                                 | $\prod$                                 |
| 微分    | \partial                              | $\partial$                              |
| 积分    | \int                                  | $\int$                                  |
| 不等于   | \neq                                  | $\neq$                                  |
| 大于等于  | \geq                                  | $\geq$                                  |
| 小于等于  | \leq                                  | $\leq$                                  |
| 约等于   | \approx                               | $\approx$                               |
| 不大于等于 | x+y \ngeq z                           | $x+y \ngeq z$                           |
| 点乘    | a \cdot b                             | $a \cdot b$                             |
| 星乘    | a \ast b                              | $a \ast b$                              |
| 分式    | \frac{b}{a}、\frac{x}{y}、\frac{1}{x+1} | $\frac{b}{a}、\frac{x}{y}、\frac{1}{x+1}$ |

## 4、数学符号

|        | markdown语法（输入在两个$$之间）       | 显示                            |
| ------ | --------------------------- | ----------------------------- |
| 无穷     | \infty                      | $\infty$                      |
| 矢量     | \vec{a}                     | $\vec{a}$                     |
| 一阶导数   | \dot{x}                     | $\dot{x}$                     |
| 二阶导数   | \ddot{x}                    | $\ddot{x}$                    |
| 算数平均值  | \bar{a}                     | $\bar{a}$                     |
| 概率分布   | \hat{a}                     | $\hat{a}$                     |
| 虚数i, j | \imath、\jmath               | $\imath、\jmath$               |
| 四种省略号  | \ldots；\cdots；\vdots；\ddots | $\ldots；\cdots；\vdots；\ddots$ |

## 5、高级运算符

|        | markdown语法（输入在两个$$之间）                                          | 显示                                                               |
| ------ | -------------------------------------------------------------- | ---------------------------------------------------------------- |
| 平均数运算  | \overline{xyz}                                                 | $\overline{xyz}$                                                 |
| 开二次方运算 | \sqrt{x}                                                       | $\sqrt{x}$                                                       |
| 开方运算   | \sqrt[开方数]{被开方数}                                               | $\sqrt[开方数]{被开方数}$                                               |
| 极限运算一  | \lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}                     | $\lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}$                     |
| 极限运算二  | \displaystyle \lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}       | $\displaystyle \lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}$       |
| 求和运算一  | \sum^{x \to \infty}_{y \to 0}{\frac{x}{y}}                     | $\sum^{x \to \infty}_{y \to 0}{\frac{x}{y}}$                     |
| 求和运算二  | \displaystyle \sum^{x \to \infty}_{y \to 0}{\frac{x}{y}}       | $\displaystyle \sum^{x \to \infty}_{y \to 0}{\frac{x}{y}}$       |
| 积分运算一  | \int^{\infty}_{0}{xdx}                                         | $\int^{\infty}_{0}{xdx}$                                         |
| 积分运算二  | \displaystyle \int^{\infty}_{0}{xdx}                           | $\displaystyle \int^{\infty}_{0}{xdx}$                           |
| 微分运算   | \frac{\partial x}{\partial y}、\frac{\partial^2x}{\partial y^2} | $\frac{\partial x}{\partial y}、\frac{\partial^2x}{\partial y^2}$ |

## 6、集合运算

|       | markdown语法（输入在两个$$之间）   | 显示                        |
| ----- | ----------------------- | ------------------------- |
| 属于    | \in                     | $\in$                     |
| 不属于   | \notin                  | $\notin$                  |
| 子集    | x \subset y、x \supset y | $x \subset y、x \supset y$ |
| 真子集   | \subseteq、\supseteq     | $\subseteq、\supseteq$     |
| 并集    | \cup                    | $\cup$                    |
| 交集    | \cap                    | $\cap$                    |
| 差集    | \setminus               | $\setminus$               |
| 同或    | \bigodot                | $\bigodot$                |
| 同与    | \bigotimes              | $\bigotimes$              |
| 异或    | \bigoplus               | $\bigoplus    $           |
| 实数集合  | \mathbb{R}              | $\mathbb{R}$              |
| 自然数集合 | \mathbb{Z}              | $\mathbb{Z}$              |

​        

## 7、其他

|      | markdown语法（输入在两个$$之间） | 显示                 |
| ---- | --------------------- | ------------------ |
| 字体变大 | \displaystyle x       | $\displaystyle x$  |
| 下划线  | \underline x          | $\underline    x$  |
| 上大括号 | \overbrace {x + y}    | $\overbrace {x+y}$ |
| 下大括号 | \underbrace{式子}       | $\underbrace{式子}$  |

## 附录：希腊字母

| 字母（大写）     | markdown语法（输入在两个$$之间） | 字母（小写）     | markdown语法 |
| ---------- | --------------------- | ---------- | ---------- |
| $\Alpha$   | \Alpha                | $\alpha$   | \alpha     |
| $\Beta$    |                       | $\beta$    | \beta      |
| $\Gamma$   | \Gamma                | $ \gamma$  | \gamma     |
| $ \Delta$  | \Delta                | \delta     | $\delta$   |
| $\Epsilon$ | \Epsilon              | $\epsilon$ | \epsilon   |
| $\Zeta$    | \Zeta                 | $\zeta$    | \zeta      |
| $\Eta$     | \Eta                  | $\eta$     | \eta       |
| $\Theta$   | \Theta                | $\theta$   | \theta     |
| $\Iota$    | \Iota                 | $\iota$    | \iota      |
| $\Kappa$   | \Kappa                | $\kappa$   | \kappa     |
| $\Lambda$  | \Lambda               | $\lambda$  | \lambda    |
| $\Mu$      | \Mu                   | $\mu$      | \mu        |
| $\Nu$      | \Nu                   | $\nu$      | \nu        |
| $\Xi$      | \Xi                   | $\xi$      | \xi        |
| $\Omicron$ | \Omicron              | $\omicron$ | \omicron   |
| $\Pi$      | \Pi                   | $\pi$      | \pi        |
| $\Rho$     | \Rho                  | $\rho$     | \rho       |
| $\Sigma$   | \Sigma                | $\sigma$   | \sigma     |
| $\Tau$     | \Tau                  | $\tau$     | \tau       |
| $\Upsilon$ | \Upsilon              | $\upsilon$ | \upsilon   |
| $\Phi$     | \Phi                  | $\phi$     | \phi       |
| $\Chi$     | \Chi                  | $\chi$     | \chi       |
| $\Psi$     | \Psi                  | $\psi$     | \psi       |
| $\Omega$   | \Omega                | $\omega$   | \omega     |