#### 1. Objects = Attibutes + Services

+ Data:the properties or status
+ Operations: the functions
+ 对象包含属性和方法
+ Object is variable in programming languages

#### 2. From the problem space to the solution one

+ 问题空间到解空间的映射
+ 把解空间描述出来的就是面向对象,解空间本身就是对象
+ 面向过程是从时间顺序来分析问题,一步一步解决问题；面向对象则是找到问题的解
+ 

#### 3. what is object-oriented

+ A way to organize
+ Designs
+ Implementations
+ + Object , not control or data flow, are the primary focus of the design and implementation
+ To focus on things, not operations

#### 4. Objects send and receive messages(object do things)

+ Messages are
  + Composed  by the sender
  + Interpreted by the receiver
  + Implemented by the methods
+ Message
  + May cause receivesr to change status
  + May return results

#### 5.  Object VS class

+ Object
  + Represent things, event or concepts
  + Respond to messages at run-time
+ Class
  + Defines propreties of instance
  + Act like type in c++
+ 对象是类的实例化,类是问题的抽象

#### 6. OOP Characteristics

+ Everthing is an Object
+ A program is a bunch of objects telling each other what to do by sending messages
+ Each object has its own memory made up of other objects
+ Every object has a type
+ All objects of a particular type can receive the same messages

#### 7. An object has an interface

+ The interface is the way it receives messages
+ It defined in the class the object belong to
+ Functions of the interface : communication, protection

#### 8. The hidden implementation

+ Inner part of an object, data members to present its state, and the actions it takes when messages is rcvd is hidden
  + Class creator VS Client programmers
+ Keeo client programmers hand off portions they should not touch 
+ Allow the class creator to change the internal working if the class without worrying about how it will affect the client programmers

#### 9. Encapsulation(封装)

+ bundle data and methods dealing with these data together in an object
+ Hide the datails of the data and the action
+ Restrict only access to the publicized methods

#### 10. Define of a class

+ In C++, separated .h and .cpp files are used to define one calss
+ Class declareation and prototypes in that class are in the header file(.h)
+ All the bodies of these functions are in the source file(.cpp)

#### 11. The header files

+ If a function is declared in a header file, you must include the header file everywhere the function is used and where the function is defined
+ If a class is declared in a header file, you must include the header file everywhere the class is used and where calss member function are defined

#### 12. Declarations VS Definetions

+ A .cpp file is a compile unit

+ Only declarations are allowed to be in .h
  
      + extern variables
      + function prototypes
      + class/struct declaration

+ .h文件中只有声明, .cpp文件中只有定义, 真正执行的是main.cpp

#### 13. Standard header file structure

```c++
#ifndef HEADER_FLAG
#define HEADER_FLAG
//type declaration here
#endif //HEADER_FLAG
```

+ 防止在一个.cpp中include同一个.h多次, 每个头文件在同一个源文件中只能被调用一次

#### 14. Tips for header

+ One class declaration per header file
+ Associatied with one source file in the same prefix of file name
+ The contents of a header file is surrounded with #ifndef #define #endif

#### 15. Abstract

+ Abstraction is the ability to ignore details of parts to focus attention on hight level of a problem
+ Modularization is process of dividing a whole into well-defined parts, which can be build and examined separated, and which interact in well-defined ways

#### 16. this指针

+ 指向当前对象地址的指针, 所有的对象都有.
  + 成员函数使用成员变量时,成员变量都会有this指针

#### 17. 对象object的地址即为该对象内所包含的第一个字段的地址。

+ 若类为空,C++裁定凡是独立对象必须有非零大小,,实际情况下为1, 编译器为空对象强制插入一个char, 所以对象的地址就是这个char的地址
+ 在继承体系中,派生类的地址:派生类对象包含了基类的成员变量和新增的成员变量, 该对象的地址就是派生类对象中所包含的基类成员中的首个字段的地址,而不是派生类自己新增的第一个字段成员的地址

#### 18. 构造函数和析构函数

> 构造函数和析构函数所有对象都有,没有特别声明时系统自动创建；

+ 构造函数
  + 函数名和类名相同
  + 没有任何返回类型,可以有参数
  + 构造函数在对象被创造时自动调用
  + 构造函数不能主动调用,在子类中只能使用initialize lis t赋值
+ 析构函数
  + 函数名是在类名前加`~` 号
  + 没有返回类型,也没有参数
  + 析构函数在对象结束时调用
    + 变量的空间在进入主函数中时已经分配,但是构造函数是在变量定义时才调用
    + 没有参数的构造器:default constructor; 编译器分配的构造器:auto default constructor
    + 利用构造函数初始化类的成员变量,要使用直接初始化,
    + + classneme::classname:data(value), ... {}

#### 19. new和delete

+ new为变量分配一片空闲的内存空间, 返回指向内存空间的地址
+ delete 释放内存空间,在这之前析构函数被调用 ,new之后必须delete
+ 利用new申请一片数组空间, delete时需要加上`[]`号
  + int* p = new int [10]; delete [] p;

#### 20. 访问限制:private, public, protected

+ to keep the client programmer’s hands off members they shouldn’t touch
+ 确保变量的安全性, 外人只能访问函数,不能随意修改变量
+ public:公开的,所有人都可以访问. 
+ private: 私有的,只有类的成员函数能够访问. 针对类而言,类的所有对象是可以访问任意对象的私有成员的
+ protected: 只有类以及类的继承能够访问, 在其他地方不能使用
+ 类的所有的成员变量都是private, 类的接口是public, 要留给继承使用的是protected

#### 21. 友元函数

+ 定义在类的外部,但是有权访问类中的私有和受保护的成员,友元函数需要在类中声明,但是友元函数不是类的成员函数
+ 友元可以是一个函数,也可以是一个类,,友元类的所有成员都是友元
+ 如果要声明一个函数是一个类的友元,需要在函数声明前使用关键字`friend`
+ 声明:`friend void func(class* ptr)`
+ 类一旦确定, 友元函数就不能再更改了

#### 22. class VS struct

+ C++中class和struct是相同的,仅有的区别如下:
+ **class** default is private
+ **struct** default is public

#### 23. 初始化

1. 直接初始化: 用圆括号来初始化变量,与拷贝初始化相比,不会储存字面量
2. 拷贝初始化: 用等号来初始化变量, 变量储存的是字面量的拷贝
3. 列表初始化: 用花括号来初始化变量, 特点是:当用于内部类型的变量时,如果初始值存在丢失信息的风险,编译器就会报错.
4. 利用构造函数初始化类的成员变量

#### 24.  interfaces: public functions

#### 25. 函数重载

+ 函数名相同但参数不相同的函数,在C++中编译器会根据参数的不同来区别函数重载
+ 返回类型不同的函数不会被重载

#### 26. 在函数声明中可以有default argument(默认实参), 在函数定义中不必再写. 在函数调用时,可以不必再传值

#### 27.  继承

> `class derived-class: access-specifier base-class`

1. access-specifier:private, public, protected;
   - 公有继承(public): 当一个类派生自公有基类时，基类的公有成员也是派生类的公有成员，基类的保护成员也是派生类的保护成员，基类的私有成员不能直接被派生类访问，但是可以通过调用基类的公有和保护成员来访问。
     - 保护继承(protected): 当一个类派生自保护基类时，基类的公有和保护成员将成为派生类的保护成员。
     - 私有继承(private): 当一个类派生自私有基类时，基类的公有和保护成员将成为派生类的私有成员。
2. Inheritance is to take the existing class , clone it, and then make additions and modifications to the clone
3. 继承也会继承基类的构造函数, 析构函数, 当基类的构造函数有参数时 需要在子类的构造函数中为其赋值
4. 在C++中基类和派生类有相同的函数,基类中的函数就会被隐藏
5. 在其他编程语言中,派生类中与基类相同函数名和参数的函数会被重写
6. 派生类会继承基类的protected和public成员,不会继承private成员
7. 派生类可以当作基类来看,
8. 对象组合: 在一个类中的成员变量可以是其他类的对象

#### 28. 内联函数

+ 内联函数在编译时就进行了替换, 而其他函数则是在执行时才进行替换,使用内联函数时注意:
  1. 内联函数不允许使用循环和开关语句
  2. 内联函数的定义必须出现在内联函数的调用之前
  3. 类中==定义==的函数都是内联函数
  4. 内联函数实质上是空间换时间的方法, 所以内联函数体一般不超过10行
  5. 函数体过大, 内联函数可能会失效
+ 关键字`inline` 在(函数声明)和函数定义前加上`inline`关键字, 那么在编译时,编译器会把该函数的代码副本放在每个函数调用的地方,在最终的可执行文件中不会有内联函数.
+ 对内联函数进行的任何修改, 都要重新编译函数的所有客户端, 因为编译器需要重新更换一次所有代码,否则将会继续使用旧的函数
+ inline函数不必有声明, inline函数的定义类似一个声明, inline函数定义应该放在.h文件里

#### 29. const

- const变量必须初始化,且一旦初始化就不可以被修改  

- 常量指针和指针常量
  
  - **常量指针**: 指针指向的对象是常量, 不可以通过指针去修改对象,*并不是说对象是常量*
  - **指针常量**: 指针是一个常量, 指针的指向不能修改
  - 区别在于const是在`*` 号的前面还是后面

- const变量不可以通过地址传递给一个非常量的指针；

- const引用不可以通过引用名去修改变量的值

- 类中成员函数加上`const`表示,这个函数不会去修改成员变量的值`void Class::func() const{}`  
  
  - `const`修饰的是this指针,具体为:`void Class::func(const A* this) const{}` 
    
    #### 30. 引用

- 引用变量是某个已存在变量的别名,可以用引用名来指向变量:`type& refname = name;`

- 不存在空引用, 引用必须指向一块合法的内存

- 一旦引用被初始化为一个对象,就不能被指向另一个对象, 

- 引用必须在创建时被初始化

- 类型必须匹配

- 将引用作为参数传递时, 在被调函数中对形参的修改也会影响主调函数的变量；类似指针,但比指针更加简洁

#### 31. 多态性(polymorphism)

- Upcast(向上造型):take an object of the derived class as an object of the base one
- 关键点:
  - 向上造型是将子类当作父类, 多态是可以通过父类的指针访问子类的成员
- 虚函数
  -