# docker

## 1. docker hello world

Docker 允许你在容器内运行应用程序

## 2. docker 镜像使用

**使用`docker images`列出所有镜像**

```shell
➜  ~ docker images
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
hello-world   latest    feb5d9fea6a5   6 months ago   13.3kB

```

各个项说明

- **REPOSITORY：**表示镜像的仓库源
- **TAG：**镜像的标签
- **IMAGE ID：**镜像ID
- **CREATED：**镜像创建时间
- **SIZE：**镜像大小

同一个仓库可以有多个tag,代表不同的版本，

**利用docker镜像创建容器**

```shell
docker run hello-world
docker run -t -i ubuntu:15.10 /bin/bash
```

各个项说明

- -i: 交互式操作。
- -t: 终端。
- ubuntu:15.10: 这是指用 ubuntu 15.10 版本镜像为基础来启动容器。
- /bin/bash：放在镜像名后的是命令，这里我们希望有个交互式 Shell，因此用的是 /bin/bash。
- 如果你不指定一个镜像的版本标签，例如你只使用 ubuntu，docker 将默认使用 ubuntu:latest 镜像。

**获取镜像**

如果使用一个不存在的镜像，那么docker会自动下载这个镜像，或者手动使用`docker pull`命令来下载镜像

```shell
docker pull hello-world
```

如果不指定版本号，将默认使用lastest镜像

**查找镜像**

- 可以从 Docker Hub 网站来搜索镜像，Docker Hub 网址为： https://hub.docker.com

- 也可以使用 `docker search` 命令来搜索镜像。比如我们需要一个 httpd 的镜像来作为我们的 web 服务。我们可以通过 docker search 命令搜索 httpd 来寻找适合我们的镜像。

  ```shell
  [~]$ docker search httpd
  NAME                                    DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
  httpd                                   The Apache HTTP Server Project                  3947      [OK]       
  centos/httpd-24-centos7                 Platform for running Apache httpd 2.4 or bui…   44                   
  centos/httpd                                                                            35                   [OK]
  hypoport/httpd-cgi                      httpd-cgi                                       2                    [OK]
  solsson/httpd-openidc                   mod_auth_openidc on official httpd image, ve…   2                    [OK]
  manageiq/httpd                          Container with httpd, built on CentOS for Ma…   1                    [OK]
  inanimate/httpd-ssl                     A play container with httpd, ssl enabled, an…   1                    [OK]
  ```

  - **NAME:** 镜像仓库源的名称
  - **DESCRIPTION:** 镜像的描述
  - **OFFICIAL:** 是否 docker 官方发布
  - **stars:** 类似 Github 里面的 star，表示点赞、喜欢的意思。
  - **AUTOMATED:** 自动构建。

**删除镜像**

使用`docker rmi`命令删除镜像

```shell
docker rmi hello-world
```

**创建镜像**

如果docker hub的镜像不能满足要求，可以通过以下两种方式对镜像进行更改。

- 从已经创建的容器中更新镜像，并且提交这个镜像
- 使用 Dockerfile 指令来创建一个新的镜像



**提交镜像**

通过命令 docker commit 来提交容器副本。

```shell
docker commit -m="update" -a="zyanjun" 48126bf66087 zyanjun/ubuntu:v2
```

参数说明

- **-m:** 提交的描述信息	
- **-a:** 指定镜像作者
- **e218edb10161：**容器 ID
- **runoob/ubuntu:v2:** 指定要创建的目标镜像名

**构建镜像**

我们使用命令 `docker build`， 从零开始来创建一个新的镜像。为此，我们需要创建一个 Dockerfile 文件，其中包含一组指令来告诉 Docker 如何构建我们的镜像。

dockerfile 的命令摘要

- FROM- 镜像从那里来

- MAINTAINER- 镜像维护者信息

- RUN- 构建镜像执行的命令，每一次RUN都会构建一层

- CMD- 容器启动的命令，如果有多个则以最后一个为准，也可以为ENTRYPOINT提供参数

- VOLUME- 定义数据卷，如果没有定义则使用默认

- USER- 指定后续执行的用户组和用户

- WORKDIR- 切换当前执行的工作目录

- HEALTHCHECH- 健康检测指令

- ARG- 变量属性值，但不在容器内部起作用

- EXPOSE- 暴露端口

- ENV- 变量属性值，容器内部也会起作用

- ADD- 添加文件，如果是压缩文件也解压

- COPY- 添加文件，以复制的形式

- ENTRYPOINT- 容器进入时执行的命令

一个简单的dockerfile文件

```dockerfile
FROM	almalinux:8.5

MAINTAINER zyanjun

RUN yum -y update && yum -y upgrade

EXPOSE 22
```

## 3. docker 容器使用

​	**启动容器**

```shell
docker run -itd --name almalinux almalinux:latest /bin/bash
```

参数说明：

- -i: 交互式操作。
- -t: 终端。
- -d: 允许后台运行，但是默认不会进入容器内，想要进入容器需要`docker exec`命令
- --name：指定容器名字
- almalinux:latest : 镜像名，不带tag默认latest
- /bin/bash：放在镜像名后的是命令，这里我们希望有个交互式 Shell，因此用的是 /bin/bash。

要退出终端，直接输入 exit.

**启动已停止的容器**

```shell
[~]$ docker ps -a      //查看所有容器                
CONTAINER ID   IMAGE       COMMAND       CREATED         STATUS         PORTS     NAMES
8b130ec71f1a   almalinux   "/bin/bash"   8 seconds ago   Exited (0) 4 seconds ago  nostalgic_moore
[~]$ docker start <容器 ID> //使用 docker start 启动一个已停止的容器
[~]$ docker stop <容器 ID>	//停止一个容器
[~]$ docker exec -it <容器 ID> /bin/bash //进入一个容器


```

使用 `docker exec`创建的容器，如果从这个容器退出，容器不会停止



**导入导出容器**

```shell
$ docker export <容器 ID> > ubuntu.tar
$ cat docker/ubuntu.tar | docker import - test/ubuntu:v1
```



**删除容器**

```shell
$ docker rm -f <容器 ID>		//删除容器
$ docker container prune	//清理掉所有处于终止状态的容器
```





## 4.Docker Compose

Compose 是用于定义和运行多容器 Docker 应用程序的工具。通过 Compose，您可以使用 YML 文件来配置应用程序需要的所有服务。然后，使用一个命令，就可以从 YML 文件配置中创建并启动所有服务。

## 4.Docker 命令

### 4.1 docker run

语法：`docker run [OPTION] IMAGE [COMMAND] [ARG...]`

**OPTION说明：**
- -a stdin: 指定标准输入输出内容类型，可选 STDIN/STDOUT/STDERR 三项；

- -d: 后台运行容器，并返回容器ID；

- -i: 以交互模式运行容器，通常与 -t 同时使用；

- -P: 随机端口映射，容器内部端口随机映射到主机的端口

- -p: 指定端口映射，格式为：主机(宿主)端口:容器端口

- -t: 为容器重新分配一个伪输入终端，通常与 -i 同时使用；

- --name="nginx-lb": 为容器指定一个名称；

- --dns 8.8.8.8: 指定容器使用的DNS服务器，默认和宿主一致；

- --dns-search example.com: 指定容器DNS搜索域名，默认和宿主一致；

- -h "mars": 指定容器的hostname；

- -e username="ritchie": 设置环境变量；

- --env-file=[]: 从指定文件读入环境变量；

- --cpuset="0-2" or --cpuset="0,1,2": 绑定容器到指定CPU运行；

- -m :设置容器使用内存最大值；

- --net="bridge": 指定容器的网络连接类型，支持 bridge/host/none/container: 四种类型；

- --link=[]: 添加链接到另一个容器；

- --expose=[]: 开放一个端口或一组端口；

- --volume , -v: 绑定一个卷



### 4.2 Docker start/stop/restart

docker start :启动一个或多个已经被停止的容器

docker stop :停止一个运行中的容器

docker restart :重启容器

**语法：**

`docker start [OPTIONS] CONTAINER [CONTAINER...]`
`docker stop [OPTIONS] CONTAINER [CONTAINER...]`
`docker restart [OPTIONS] CONTAINER [CONTAINER...]`

### 4.3 docker kill

docker kill 杀掉一个运行中的容器

**语法**

`docker kill [OPTIONS] CONTAINER [CONTAINER...]`

- -s:向容器发送一个信号


### 4.4 docker rm

docker rm ：删除一个或多个容器。

**语法**

`docker rm [OPTIONS] CONTAINER [CONTAINER...]`

**option**

- -f :通过 SIGKILL 信号强制删除一个运行中的容器。

- -l :移除容器间的网络连接，而非容器本身。

- -v :删除与容器关联的卷。


### 4.5 docker exec

在运行中的容器中执行命令

**语法：**
`docker exec [OPTIONS] CONTAINER COMMAND [ARG...]`

**option**

- d :分离模式: 在后台运行
-
- -i :即使没有附加也保持STDIN 打开
-
- -t :分配一个伪终端

### 4.6 docker ps

`docker ps `:列出容器

**语法：**

`docker ps [OPTION]`

**OPTION**
- -a :显示所有的容器，包括未运行的。
- -f :根据条件过滤显示的内容。
- --format :指定返回值的模板文件。
- -l :显示最近创建的容器。
- -n :列出最近创建的n个容器。
- --no-trunc :不截断输出。
- -q :静默模式，只显示容器编号。
- -s :显示总的文件大小。

**实例**

```shell
[~]$ docker ps -a
CONTAINER ID   IMAGE       COMMAND       CREATED        STATUS                    PORTS     NAMES
61eed7f3cfab   almalinux   "/bin/bash"   30 hours ago   Exited (0) 28 hours ago             almalinux
```

**输出详情**

输出详情介绍：

CONTAINER ID: 容器 ID。

IMAGE: 使用的镜像。

COMMAND: 启动容器时运行的命令。

CREATED: 容器的创建时间。

STATUS: 容器状态。

状态有7种：

    created（已创建）
    restarting（重启中）
    running（运行中）
    removing（迁移中）
    paused（暂停）
    exited（停止）
    dead（死亡）

PORTS: 容器的端口信息和使用的连接类型（tcp\udp）。

NAMES: 自动分配的容器名称。

### 4.7 docker top

`docker top`:查看容器中运行的进程信息，支持 ps 命令参数。
容器运行时不一定有/bin/bash终端来交互执行top命令，而且容器还不一定有top命令，可以使用docker top来实现查看container中正在运行的进程。

**语法**

`docker top [OPTIONS] CONTAINER [ps OPTIONS]`


### 4.8 docker export

将容器文件系统导出到STDOUT

**语法**
`docker export [OPTION] CONTAINER`
- -o:将输入内容写入到文件

`docker export -o almalinux-`date +%Y%m%d`.tar 61eed7f3cfab`

### 4.8.1 docker import

从归档文件创建镜像

`docker import [OPTIONS] file [REPOSITORY[:TAG]]`

**OPTION**

- -c:应用docker指令创建镜像
- -m：提交时的说明文字


### 4.9 docker commit

利用存在的容器创建镜像

`docker commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]`
**OPTION**
- -a :提交的镜像作者；

- -c :使用Dockerfile指令来创建镜像；

- -m :提交时的说明文字；

- -p :在commit时，将容器暂停。


### 4.10 docker cp

用于在主机和容器中的文件复制

`docker cp [OPTION] CONTAINER:SRC_PATH DEST_PATH`
`docker cp [OPTION] DEST_PATH CONTAINER:SRC_PATH`

**OPTION**
- -L:保持源目录中的软链接


### 4.12 docker login/logout

`docker login -u username -p passwd`:登陆dockerhub
`docker logout`:登出

### 4.13 docker pull

从docker hub拉取镜像

`docker pull java`:拉取java镜像到本地

### 4.14 docker push

将本地docker镜像上传到dockerhub,需要登陆

`docker push [OPTIONS] NAME[:TAG]`

### 4.15 docker images

列出本地镜像

`docker images [OPTIONS] [REPOSITORY[:TAG]]`

**OPTION**
- -a :列出本地所有的镜像（含中间映像层，默认情况下，过滤掉中间映像层）；

- --digests :显示镜像的摘要信息；

- -f :显示满足条件的镜像；

- --format :指定返回值的模板文件；

- --no-trunc :显示完整的镜像信息；

- -q :只显示镜像ID。

### 4.16 docker rmi

删除image

`docker rmi [OPTIONS] IMAGE [IMAGE...]`
**OPTION**
- -f:强制删除

- --no-prune :不移除该镜像的过程镜像，默认移除；

### 4.17 docker save

将本地镜像保存为tar归档文件

`docker save [OPTION] IMAGE`

- -o:指定文件名

`docker save -o almalinux.tar alamlinux`

### 4.18 docker load

导入由docker save创建的镜像

`docker load [OPTION]`

**OPTION**

- --input , -i : 指定导入的文件，代替 STDIN。

- --quiet , -q : 精简输出信息。


