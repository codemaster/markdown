## 一.基础配置

#### 一. 基础知识

+ 在命令行模式下输入配置命令，会在本次编辑中临时生效；
+ Vim的全局配置在 ==/etc/vim/vimrc== 或 ==/etc/vimrc== 中，对所有用户生效。个人配置在==~/.vimrc==。
+ 配置项有打开、关闭两种选项，关闭就是在打开命令前加 ’ no '
+ `set 命令？`查询命令是打开还是关闭
+ `命令 help` 查看帮助

#### 二. 配置

+ `set showmatch` 光标遇到圆括号、方括号、大括号时，自动高亮对应的括号；
+ `set syntax on` 打开语法高亮，自动识别代码；
+ `set showmode` 在底部显示命令模式或 插入模式；
+ `set showcmd` 在底部显示当前键入的命令；
+ `set mouse=a` 支持鼠标；
+ `filetype indent on` 开启文件类型检查，并配置相应的缩进规则；
+ `autocmd BufWritePost \$MYVIMRC source $MYVIMRC`使配置立即生效；

#### 三. 缩进

+ `set autoindent` 继承前一行的缩进方式；
+ `set tabstop=2` tab键表示的空格数；
+ `set shiftwidth=4` 每一次缩进的字符数；
+ `set smartindent`自动缩进；

#### 四.外观

+ `set num`设置行号；
+ `set cursorline`光标所在行高亮；
+ `set laststatus=2`是否显示状态栏，0表示不显示，1表示只在多窗口显示，2表示显示；
+ `set ruler`在状态栏显示光标当前位置；

#### 五.编辑

+ `set spell spelllang=en_us`打开英语单词的拼写检查；
+ `set nobackup`不创建备份文件，默认在文件保存时会创建文件名为源文件名加波浪号（~）的备份文件；
+ `set noswapfile`不创建交换文件，交换文件用于在系统崩溃时恢复文件，以==.swp==结尾

## 二.vim常用命令

+ i/o 切换到输入模式；
+ ESC 返回命令模式；
+ ：切换底线命令模式；
+ yy 复制当前行、p 粘贴
+ gg到第一行、G到最后一行；
+ $到行首、0到行尾；
+ x删除光标所在字符、dd剪切所在行
+ u撤销、n到第n行
+ w保存、q退出、wq保存退出、ZZ保存退出
+ 