## 第一天

- 特点
  - 是一种面向对象的语言
  - 使用引用来替代指针
  - 提供了方便的内存回收机制
  -  提供良好的可移植性
  
- java可移植性：同一个程序可以在不同的平台上运行

- java应用程序机制
  - 计算机高级编程语言类型：编译型、解释型
  - java是两种语言的结合：编译命令：`javac.exe`、解释命令：`java.exe`
  - java程序组成：java源文件(.java)、字节码文件(.class)、机器码文件(二进制文件)
  - 所有java程序的解释都要求放在java虚拟机上处理
  
- java虚拟机：JVM（Java Virtual Machine）
  - JVM是由一个软件和硬件模拟出来的计算机，程序只要有java虚拟机的支持就可以实现程序的执行
  - Java虚拟机读取并处理经过编译的字节码文件
  - Java编译器针对java虚拟机产生calss文件，因此是独立平台的
  - java解释器负责将java虚拟机的代码在特定的平台运行（比如JVM for linux）
  - class文件只能通过JVM运行，由JVM去适应各个平台
  
- 与C/C++的对比：C/C++代码编译之后产生可执行文件，可以直接运行；而Java经过编译产生字节码文件，只能在JVM上运行，因此Java程序运行较慢

- Java开发环境搭建

  - 安装JDK：Java Development Kit, java开发包
  - 安装JRE：Java Runtime Enviroment, java运行时环境（只提供程序的解释功能，安装JDK时自动安装）
  - SDK：Software Devolopment Kit, 软件开发包

- 第一个程序

  - 编辑源代码：nvim Hello.java

    ```java
     public class HelloWorld{
         Public static void main(String argc[]){
             System.out.print("hello world！"); //输出之后不换行
             System.out.println("hello world！"); //输出之后换行
         } 
     }
    ```

  - 编译源代码文件：javac Hello.java, 生成.class后缀的字节码文件

  - 解释执行字节码文件(在JVM上)：java Hello，**注意**：解释字节码文件不需要加后缀.class，但是此时解释的就是Hello.class文件 

  - 程序解释

    - java大小写敏感、类名每个单词首字母大写

    - java开发最基础的单元就是类，所有的程序封装在类中执行 

      - `[public] class 类名 {}`：类名要与文件名一致，所以一个java文件只能有一个public类

      - `class 类名 {}`：类名可以与文件名不一致，但是编译生成的字节码文件的名称是类名.class而不是文件名.class 。如果由多个类，那么每个类会生成一个相对应的字节码文件
      - **注意**：一个java文件中默认只有一个public类，不建议有多个类
      - 类包括属性和方法，属性就是变量、方法就是函数

    - 主方法：

      - `public static void  main(String argc[])`：主方法一定要有，方括号可前可后
      - 主方法是所有程序执行的起点，并且一定要**定义**在类中,主方法所在的类一般称为主类，所有的主类都用public声明

- jshell：  java脚本，可以执行一些简单的java代码而无需编写完成的java文件，有利于进行简短的程序验证，可用性一般

- CLASSPATH:

  - 由JRE提供的，用于定义java程序解释时类加载路径，默认为当前所在路径 
  - JVM -> CLASSPATH定义的路径 -> 加载字节码文件
  - SET CALSSPATH=字节码文件目录。手动设置CLASSPATH，不建议配置 

- 注释：`//`：单行注释；`/*...*/`：多行注释

- 标识符： 用于结构说明，标识符由字母、数字、下划线、$符组成，不能使用java关键字，建议驼峰命名法

