# vim 插件目录

## 1. vim-plug

+ `:PlugInstall` 安装插件. 需要先在`.vimrc`中添加相应字段
+ `:PlugUpdate` 升级插件
+ `:Plugupgrade` 升级vim-plug
+ `:PlugStatus` 查看插件状态

## 2. nerdtree

+ 在终端进入项目文件夹, 输入vim .,即可看到树形目录.
  **快捷键**

+ h j k l移动光标定位

+ ctrl+w+w 光标在左右窗口切换

+ ctrl+w+r 切换当前窗口左右布局

+ ctrl+p 模糊搜索文件

+ o 打开关闭文件或者目录，如果是文件的话，光标出现在打开的文件中

+ O 打开结点下的所有目录

+ X 合拢当前结点的所有目录

+ x 合拢当前结点的父目录

+ i和s水平分割或纵向分割窗口打开文件

+ u 打开上层目录

+ t 在标签页中打开

+ T 在后台标签页中打开

+ p 到上层目录

+ P 到根目录

+ K 到同目录第一个节点

+ J 到同目录最后一个节点

+ m 显示文件系统菜单（添加、删除、移动操作）

+ ? 帮助

+ :q 关闭
  
  ## 3.LeaderF

+ 搜索当前目录下的文件
  :LeaderfFile
  <leader>f

+ 搜索当前的Buffer
  :LeaderfBuffer
  <leader>b

+ 搜索最近使用过的文件( search most recently used files)就是Mru
  :LeaderfMru

+ 搜索当前文件的函数（相当强啊）
  :LeaderfFunction

+ 搜索当前文件中有的某个单词（好处就是能把他们都列出来，不是很常用，其实，不过可以看看有多少行，也不错）
  :LeaderfLine
