+ Q`alt+num` : 切换工作区
+ `alt+e` : 切换水平/垂直分配窗格
+ `alt+s`: (stacking)工作区里只显示一个窗格, 在工作区的上方显示其他窗格的列表
+ `alt+w` : (tabbed) 跟stacking类似, 不过窗格列表是单行的标签页

+ `alt+f`: 全屏模式
+ `alt+d/q`: 程序启动器
+ `alt+shift+q` : 关闭窗格
+ `alt+shift+num` : 移动窗格到另一个工作区
+ `alt+shift+space` : 切换窗格为浮动模式
+ `alt+up/down/left/right`:切换窗格
+ `alt+shift+s` : 休眠
+ `alt+shift+e` : 退出i3
+ `alt+shift+r` : 重启i3
+ 

